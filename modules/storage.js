var azure = require('azure-storage');

var account = process.env.AZURE_STORAGE_ACCOUNT;
var key = process.env.AZURE_STORAGE_ACCESS_KEY;
var tableName = process.env.TABLE_NAME;
var partitionKey = process.env.PARTITION_KEY;

var tableService = azure.createTableService(account, key);
tableService.createTableIfNotExists(tableName, function(error, result, response) {
  if (!error) {
    console.log(result);
  }
});

var parseScoreJson = function parseScoreJson (json) {
  var array = [];

  for (var doc in json) {
    // console.log(json[doc]);
    array.push(String(json[doc].score._));
  }

  return array;
}

var storageJson = {
  tableName: tableName,
  partitionKey: partitionKey,
  entGen: azure.TableUtilities.entityGenerator,
  query: azure.TableQuery,
  tableSvc: tableService,
  parseScoreJson: parseScoreJson
};

module.exports = storageJson;