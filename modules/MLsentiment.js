var request = require('request');

module.exports = function (scores, callback) {
    scores.push('0');
    var options = {
        uri: 'https://ussouthcentral.services.azureml.net/workspaces/8ef3963a1adb48dc838478537418747b/services/95bbd0d83f6341e4a874ea09bb2f523b/execute?api-version=2.0&details=true',
        json: true,
        headers: {
            'Authorization': 'Bearer 0dZjIeckYFiyCmT2qSze8l+lxqhIDzCEMOCEDu+RrNvjsXNtYoZdApkauQLF5vMH5i0dh32Wz649Nhnmo8G1xQ==',
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        method: 'POST',
        body: {
            'Inputs': {
                'input1': {
                    'ColumnNames': [
                        '1st Day',
                        '2nd Day',
                        '3rd Day',
                        '4th Day',
                        '5th Day',
                        '6th Day',
                        '7th Day',
                        'Result'
                    ],
                    'Values': [ 
                        scores
                    ]
                }
            },
            "GlobalParameters": {}
        }

    };

    request.post(options, callback);
    /*function (error, body, response) {
        if (error) {
            console.log(error);
            result = error;
        }
        else {
            console.log(body.body.Results.output1.value.Values[0][0]);
        }
    }*/
}