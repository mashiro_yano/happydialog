var request = require('request');
const uuidv1 = require('uuid/v1');

module.exports = function analytics(text, callback) {
    var options = {
        uri: 'https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment',
        json: true,
        headers: {
            'Ocp-Apim-Subscription-Key': process.env.COGNITIVE_SERVICE_SUBSCRIPTION_KEY,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        method: 'POST',
        body: {
            'documents':[
            {
                'language': 'en',
                'id': uuidv1(),
                'text': text
            }]
        }
    };

    request.post(options, callback);
}