var express = require('express');
var ml = require('../modules/MLsentiment');
var storage = require('../modules/storage');
var router = express.Router();

/* Weekly happiness. */
router.get('/', function(req, res, next) {
  // DBから1週間のデータを持ってくる処理
  var query = new storage.query().top(7).where('PartitionKey eq ?', storage.partitionKey).select('score');
  storage.tableSvc.queryEntities(storage.tableName, query, null, function (error, result, response) {
    if (!error) {
      var scoreJson = storage.parseScoreJson(result.entries);
      ml(scoreJson, function (error, body, response) {
        if (!error) {
          res.send({sentiment: body.body.Results.output1.value.Values[0][0]});
        }
        else {
          console.log(error);
          res.send(error);
        }
      });
    }
    else {
      console.log(error);
      res.send(error);
    }
  });
});

module.exports = router;
