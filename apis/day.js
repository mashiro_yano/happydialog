var express = require('express');
var cognitive = require('../modules/sentiment');
var storage = require('../modules/storage');
var router = express.Router();

var count = 9999999999;

/* 1 Day Diary */
router.post('/', function(req, res, next) {
  // 受け取ったテキストを変数に入れる
  var text = req.body['text'];

  cognitive(text, function (error, response, body) {
    if (error){
      console.log(error);
      res.send(error);
    }
    else{
      var cognitiveResult = body.documents[0];
      
      //Storageにリザルトをインサート
      var entity = {
        PartitionKey: storage.entGen.String(storage.partitionKey),
        RowKey: storage.entGen.String(String(count--) + '-' + cognitiveResult.id),
        text: storage.entGen.String(text),
        score: storage.entGen.Double(cognitiveResult.score)
      };
    
      storage.tableSvc.insertEntity(storage.tableName, entity, function (error, result, response) {
        if (!error) {
          var resJson = {
            sentiment: cognitiveResult.score,
            text: text
          }
          res.send(resJson);
        }
        else {
          console.log(error);
          res.send(error);
        }
      });
    }
  });
});

module.exports = router;