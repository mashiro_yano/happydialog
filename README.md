# happydialog v1.0
app.jsと同じフォルダに.envファイルを作成し、AzureStorageのアカウントと接続キー、Text Analytics APIの接続キー、Azure Storage Tableのテーブル名とPartitionKeyを設定してください。

	AZURE_STORAGE_ACCOUNT = "account-name"
	AZURE_STORAGE_ACCESS_KEY = "access-key"
	COGNITIVE_SERVICE_SUBSCRIPTION_KEY = "subscriptionkey"
	TABLE_NAME = "tablename"
	PARTITION_KEY = "partitiokey"

テーブルが無い場合は自動的に作成されます。